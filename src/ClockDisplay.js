import React from "react";
import ClockUI from "./ClockUI";

class ClockDisplay extends React.Component {
  state = { time: new Date().toLocaleTimeString() };
  componentDidMount() {
    setInterval(() => {
      this.setState({ time: new Date().toLocaleTimeString() });
    }, 1000);
  }
  render(){
    return <ClockUI time={this.state.time} />;
  }
}

export default ClockDisplay;
