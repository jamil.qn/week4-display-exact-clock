import React from "react";
import ReactDOM from "react-dom";
import ClockDisplay from "./ClockDisplay";

ReactDOM.render(<ClockDisplay />, document.querySelector("#root"));
