import React from "react";
import "./ClockUI.css";
import AMICon from "./AM.png";
import PMICon from "./PM.png";

const clockConfig = {
  AM: {
    iconAdress: AMICon,
  },
  PM: {
    iconAdress: PMICon,
  },
};

const ClockUI = ({ time }) => {
  const result = time.indexOf("PM") ? "PM" : "AM";
  const { iconAdress } = clockConfig[result];
  return (
    <div
      className="clock-body"
      style={{ backgroundImage: `url(${iconAdress})` }}
    >
      <div className={`clock-box ${result}`}>
        <h1>{time}</h1>
      </div>
    </div>
  );
};

export default ClockUI;
